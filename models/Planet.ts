export interface Planet {
  id: string;
  name: string;
  description: string;
  type: string;
  mass: string;
  diameter: string;
  gravity: string;
  rotation: string;
  traslation: string;
  distance: string;
  moons: number;
  image: string;
}
